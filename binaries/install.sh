#!/bin/bash -e

BASE_GIT_URL="${ROOT_REPO}"
LIST_OF_REPOS=(command_menu command_executor desktop_scripts screen_lock window_manager)
LIST_OF_CLONES=()
BIN_INSTALL_PREFIX="/usr/bin"

for repo in ${LIST_OF_REPOS[*]}; do
  LIST_OF_CLONES+=($BASE_GIT_URL/$repo)
  rm -frv $repo
done

for clone in ${LIST_OF_CLONES[*]}; do
  git clone $clone;
done

LIST_OF_INSTALL=(screen_lock command_executor)

for bin_to_install in ${LIST_OF_INSTALL[*]}; do
  pushd $bin_to_install
    mkdir -p build
    pushd build
      cmake -DCMAKE_INSTALL_PREFIX=/usr/bin/ .. && \
      sudo make install && \
      sudo chown root $BIN_INSTALL_PREFIX/$bin_to_install && \
      sudo chmod u+s $BIN_INSTALL_PREFIX/$bin_to_install
    popd
  popd
done

pushd window_manager
  sudo make install
popd

pushd command_menu
  sudo make install
  sudo ln -sf /usr/local/bin/dmenu_run /bin/dmenu_run
  sudo ln -sf /usr/local/bin/dmenu_path /bin/dmenu_path
popd

pushd /tmp
  git clone https://github.com/grwlf/xkb-switch
  pushd xkb-switch
    cmake -S . -B build -DCMAKE_INSTALL_PREFIX=/usr
    cmake --build build
    sudo -E cmake --install build
  popd
  rm -frv xkb-switch
popd

pushd desktop_scripts
  chmod +x *.sh
popd
cp -Rr desktop_scripts ${HOME}/.dwm
sudo ln -sf ${HOME}/.dwm /bin/.dwm

for repo in ${LIST_OF_REPOS[*]}; do
  rm -frv $repo
done
