#!/bin/bash -e

function install_aur_package {
  local package_name="$1"
  pushd ${PWD}
    pushd /tmp
      git clone https://aur.archlinux.org/${package_name}.git
      pushd ${package_name}
        makepkg -si --noconfirm
      popd
      rm -frv ${package_name}
    popd
  popd
}

if [[ -e ${DISTRO} ]]; then
  rm -frv ${DISTRO}
fi

git clone ${ROOT_REPO}/distro_packages -b ${DISTRO} ${DISTRO} 

pushd ${DISTRO}
  pushd desktop

    package_command -Syy 
    package_command --noconfirm -S `cat packages.txt` --overwrite '*'
    package_command --noconfirm -Syu 

    if [ 0 -eq `id -u` ]; then
      # It is not allowed to run makepkg
      # for root user, so quit before it is too late.
      popd
      exit 0
    fi
    # Here some packages may not be built due 
    # to not regular dependency updates, so do not 
    # fail all if one fails.
    set +e

    for p in `cat aur_packages.txt`; do
      install_aur_package $p
    done
  popd
popd

rm -frv ${DISTRO}
