#!/bin/bash -e

function install_universal_ctags {
  pushd ${PWD}
    pushd ${HOME}
      git clone https://github.com/universal-ctags/ctags
      pushd ctags
        ./autogen.sh
        ./configure
        make
        sudo make install
      popd
      rm -frv ctags
    popd
  popd
}

if [[ -e ${DISTRO} ]]; then
  rm -frv ${DISTRO}
fi

git clone ${ROOT_REPO}/distro_packages -b ${DISTRO} ${DISTRO} 

pushd ${DISTRO}
  pushd desktop
    export DEBIAN_FRONTEND=noninteractive 
    repo_package_command `cat repos.txt`
    package_command update
    package_command install -yq `cat packages.txt`
  popd
popd

rm -frv ${DISTRO}
