#!/bin/bash -e

if [[ "${DISTRO}" == "archlinux" ]]; then
	echo [multilib] | tee -a /etc/pacman.conf
	echo Include = /etc/pacman.d/mirrorlist | tee -a /etc/pacman.conf
	pacman-key --init
	pacman-key --populate archlinux
	pacman -Syy 
	pacman -S --noconfirm archlinux-keyring 
	pacman -Syy 
	pacman -S --noconfirm sudo git glibc lib32-glibc
fi

if [[ "${DISTRO}" == "ubuntu" ]] || [[ "${DISTRO}" == "debian" ]]; then
	apt update && apt install -yq sudo git software-properties-common
fi
