#!/bin/bash

if [[ "${DISTRO}" == "archlinux" ]]; then
	function package_command {
		if [ 0 -eq `id -u` ]; then
			pacman "$@"
		else
			echo $PASSWORD | sudo -S pacman "$@"
		fi
	}

	export -f package_command
fi

if [[ "${DISTRO}" == "ubuntu" ]] || [[ "${DISTRO}" == "debian" ]]; then
	function package_command {
		if [ 0 -eq `id -u` ]; then
			apt "$@"
		else
			echo $PASSWORD | sudo -S apt "$@"
		fi
	}

	function repo_package_command {
		if [ 0 -eq `id -u` ]; then
			add-apt-repository "$@"
		else
			echo $PASSWORD | sudo -S add-apt-repository "$@"
		fi
	}

	export -f package_command
	export -f repo_package_command
fi
