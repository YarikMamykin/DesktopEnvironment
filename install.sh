#!/bin/bash -e

AVAILABLE_DISTROS="archlinux ubuntu debian"

if [ -z ${DISTRO} ]; then 
	printf '%s %s: ' "DISTRO env variable is not specified!" "Specify from this list"
	for each in ${AVAILABLE_DISTROS}; do
		printf '%s ' $each
	done
	printf '\n'
	exit 1;
fi

if [ -z ${ROOT_REPO} ]; then 
	printf '%s\n' 'Specify source repo where to pull packages and binaries'
	exit 1;
fi

source ./common.sh 

pushd packages
	./install_${DISTRO}_pkgs.sh
popd

pushd binaries
	./install.sh
popd

pushd xorg
	if [ 0 -eq `id -u` ]; then
		cp 70-synaptics.conf /etc/X11/xorg.conf.d/
	else
		sudo cp 70-synaptics.conf /etc/X11/xorg.conf.d/
	fi
popd
